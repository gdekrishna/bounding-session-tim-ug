## Laravel Boilerplate (Current: Laravel 6.0) ([Demo](http://134.209.123.206/))

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-boilerplate)
[![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-boilerplate)
<br/>
[![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://styleci.io/repos/30171828/shield?style=plastic)
[![CircleCI](https://circleci.com/gh/rappasoft/laravel-boilerplate/tree/master.svg?style=svg)](https://circleci.com/gh/rappasoft/laravel-boilerplate/tree/master)
<br/>
![GitHub contributors](https://img.shields.io/github/contributors/rappasoft/laravel-boilerplate.svg)
![GitHub stars](https://img.shields.io/github/stars/rappasoft/laravel-boilerplate.svg?style=social)

### Demo Credentials

**User:** admin@admin.com  
**Password:** secret

### Official Documentation

[Click here for the official documentation](http://laravel-boilerplate.com)

### Quick Startup

-- set .env file <br/>
-- composer install <br/>
-- npm install atau yarn <br/>
-- php artisan key:generate <br/>
-- php artisan migrate <br/>
-- php artisan db:seed (ngisi db buat akun2) <br/><br/>

coba cek appnya udh jalan bagus blm tampilannya, kalo blm lengkapin jsnya pake npm <br/>
-- npm run (pilih watch aja keknya deh wkwk (npm run watch)) <br/><br/>

-- php artisan storage:link <br/><br/>

done <br/><br/>

buat set2 fitur ada di config/access, baca dokumentasi fitur <br/>
