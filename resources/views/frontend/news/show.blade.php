<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>News - {{$new->judul}}</title>
  </head>
  <body>
    <h4>{{$new->judul}}</h4>
    <small>
      created by : {{$new->user->first_name . ' '}}
      on {{$new->created_at}}
      {{-- tombol update --}}
      <a href="{{route('news.edit',[$new->id])}}">
        <button type="button" name="editButton">edit</button>
      </a>
      {{-- tombol delete --}}
      <form onsubmit="del()" class="" action="{!! route('news.destroy',[$new->id]) !!}" method="post">
        @method('DELETE')
        @csrf
        <button type="submit" name="delete">delete</button>
      </form>
      {{-- <a href="#">
        <button type="submit" onclick="del()" name="deleteButton">delete</button>
      </a> --}}

    </small>
      <p>
          {!!$new->isi!!}
      </p> <br>

      <script>
        function del() {
            confirm("Are you sure to delete this article?");
        }
    </script>
  </body>
</html>
