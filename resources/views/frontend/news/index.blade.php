<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>News</title>
  </head>
  <body>
    <h2>News List</h2>
    <a href="{{route('news.create')}}">
      <button type="button" name="create">Create News</button>
    </a>
    @foreach ($data as $datum)
      <hr>
      <h3>
        {{-- judul --}}
        <a href="{{route('news.show',[$datum->slug])}}">
          {{$datum->judul}} <br>
        </a>
        <small>
          created by : {{$datum->user->first_name . ' '}}
          on {{$datum->created_at}}
        </small>
      </h3>
      <p>

        {{ Str::limit($datum->isi, $limit = 100, $end = '...')}}
        {{-- <a href="{{route('news.show',[$datum->slug])}}">read more</a> --}}
      </p> <br>
    @endforeach
    {{$data->links()}}
  </body>
</html>
