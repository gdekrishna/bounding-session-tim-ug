<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Editing an Article</title>
  </head>
  <body>
    <h2>Revising News</h2>
    <form class="" action="{{route('news.update',[$new->id])}}" method="post">
      @method('PUT')
      @csrf
      Author : <br>
      <input type="text" name="author" value="{{Auth::user()->first_name}}" readonly> <br>
      Title : <br>
      <input type="text" name="title" placeholder="Title of The News" value="{{old('title',$new->judul)}}" maxlength="120"> <br>
      Content : <br>
      <textarea name="content" rows="8" cols="80" placeholder="Content of The News">{!!old('content',$new->isi)!!}</textarea>
      <br><br>
      <button type="submit" name="">Revise</button>
    </form>
  </body>
</html>
