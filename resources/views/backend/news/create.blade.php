<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>News</title>
  </head>
  <body>
    <h2>Insert News</h2>
    <form class="" action="{{route('news.store')}}" method="post">
      @csrf
      Author : <br>
      <input type="text" name="author" value="{{Auth::user()->first_name}}" readonly> <br>
      Title : <br>
      <input type="text" name="title" placeholder="Title of The News" value="{{old('title','')}}" maxlength="120"> <br>
      Content : <br>
      <textarea name="content" rows="8" cols="80" placeholder="Content of The News">{{old('content','')}}</textarea>
      <br><br>
      <button type="submit" name="">Publish</button>
    </form>
  </body>
</html>
