<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    // declare nama tabel
    protected $table = 'news';
    // declare primary key
    protected $primaryKey = 'id';
    // // // set incrementing off
    // // public $incrementing = false;
    // // declare primary key type
    // protected $keyType = 'string';
    // declare blacklist for mass assignable
    protected $guarded = [];

    // relasi dengan table user
    public function user(){
      return $this->belongsTo('App\Models\Auth\User');
    }

}
