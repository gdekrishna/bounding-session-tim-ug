<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = News::orderBy('created_at','desc')->paginate(8);
        return view('frontend.news.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slug = Str_slug($request->title,'-');
        // dd($slug);
        if(News::where('slug',$slug)->first()!=null)
        $slug = $slug.'-'.time();
        // dd($request->konten);
        $data = News::create([
          'user_id' => Auth::user()->id,
          'judul' => $request->title,
          'slug' => $slug,
          'isi' => $request->content,
        ]);
        return redirect(route('news.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  str  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $new = News::where('slug',$slug)->first();
        if(empty($new))
        abort(404,'message');
        return view('frontend.news.show',compact('new'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new = News::findOrFail($id);
        return view('backend.news.edit',compact('new'));
        // dd($new);
        // if(empty($new))
        // abort(404,'Something wrong happens with your request:(');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = News::findOrFail($id);
        $slug = Str_slug($request->title,'-');
        // dd($slug);
        if(News::where('slug',$slug)->first()!=null)
        $slug = $slug.'-'.time();

        $update->update([
          'user_id' => Auth::user()->id,
          'judul' => $request->title,
          'slug' => $slug,
          'isi' => $request->content,
        ]);

        return redirect(route('news.show',[$slug]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $destroy = News::findOrFail($id);
        $destroy->delete();
        return redirect(route('news.index'));
    }
}
